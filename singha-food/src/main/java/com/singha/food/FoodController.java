package com.singha.food;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/food")
public class FoodController {
	
	@GetMapping
	public ResponseEntity get() {
		return ResponseEntity.ok("food get service");
	}
}
