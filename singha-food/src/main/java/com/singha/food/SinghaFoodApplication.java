package com.singha.food;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SinghaFoodApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinghaFoodApplication.class, args);
	}

}
