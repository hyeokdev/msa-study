package com.singha.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SinghaEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinghaEurekaApplication.class, args);
	}

}
