package com.singha.user;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@GetMapping("{uid}")
	public ResponseEntity get(@PathVariable("uid") String uid) {
		return ResponseEntity.ok("user uid: " + uid);
	}
}
