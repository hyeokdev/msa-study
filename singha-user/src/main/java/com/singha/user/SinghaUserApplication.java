package com.singha.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SinghaUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinghaUserApplication.class, args);
	}

}
